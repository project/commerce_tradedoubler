<?php

/**
 * @file
 * This file contains no working PHP code; it exists to provide additional
 * documentation for doxygen as well as to document hooks in the standard
 * Drupal manner.
 */

/**
 * Allows modules to change the container tag config object.
 *
 * @param $type
 *   Container tag type: frontpage, product_listing, product, basket, registration, post_purchase.
 * @param $config
 *   Already processed config object according to main settings.
 * @param $nodes
 *   All node(s) being processed. Valid only for product_listing and product type.
 * @param $order
 *   Order being processed. Valid only for basket and post_purchase types.
 *
 * @return
 *   A config object @see includes/commerce_tradedoubler.module to know the structure.
 */
function hook_commerce_tradedoubler_config_alter($type, $config, $nodes, $order) {
  if ($type == 'product') {
    $config->imageUrl = 'http://example.com/image.jpg';
  }
}