<?php
/**
 * @file
 * Functions only needed on configuration pages.
 */

function commerce_tradedoubler_settings_form($form, &$form_state) {
  $form = array();

  $form['tradedoubler'] = array(
    '#type' => 'vertical_tabs'
  );

  $container_tag_settings = commerce_tradedoubler_get_container_tags_settings();

  foreach ($container_tag_settings as $key => $value) {
    $form[$key . '_settings'] = array(
      '#type' => 'fieldset',
      '#title' => $value['title'],
      '#group' => 'tradedoubler'
    );

    $form[$key . '_settings']['commerce_tradedoubler_' . $key . '_container_tag_id'] = array(
      '#type' => 'textfield',
      '#title' => t('Container tag ID'),
      '#default_value' => variable_get('commerce_tradedoubler_' . $key . '_container_tag_id', '')
    );

    $form[$key . '_settings']['commerce_tradedoubler_' . $key . '_allowed_pages'] = array(
      '#type' => 'textarea',
      '#title' => t('Allowed pages'),
      '#description' => t('Add one path per line.'),
      '#default_value' => variable_get('commerce_tradedoubler_' . $key . '_allowed_pages', '')
    );

    $form[$key . '_settings']['commerce_tradedoubler_' . $key . '_excluded_pages'] = array(
      '#type' => 'textarea',
      '#title' => t('Excluded pages'),
      '#description' => t('Add one path per line.'),
      '#default_value' => variable_get('commerce_tradedoubler_' . $key . '_excluded_pages', '')
    );

  }

  $form['mapping_product'] = array(
    '#type' => 'fieldset',
    '#title' => t('Product fields mapping'),
    '#group' => 'tradedoubler'
  );

  $form['mapping_product']['mapping_description'] = array(
    '#markup' => '<p>' . t('Use the field tokens below to map with TradeDoubler properties.') . '</p>',
  );

  $form['mapping_product']['commerce_tradedoubler_category_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Category name (product listing)'),
    '#maxlength' => 256,
    '#default_value' => variable_get('commerce_tradedoubler_category_name', '')
  );

  $form['mapping_product']['commerce_tradedoubler_product_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Product ID'),
    '#maxlength' => 256,
    '#default_value' => variable_get('commerce_tradedoubler_product_id', '')
  );

  $form['mapping_product']['commerce_tradedoubler_price'] = array(
    '#type' => 'textfield',
    '#title' => t('Price'),
    '#maxlength' => 256,
    '#default_value' => variable_get('commerce_tradedoubler_price', '')
  );

  $form['mapping_product']['commerce_tradedoubler_currency'] = array(
    '#type' => 'textfield',
    '#title' => t('Currency'),
    '#maxlength' => 256,
    '#default_value' => variable_get('commerce_tradedoubler_currency', '')
  );

  $form['mapping_product']['commerce_tradedoubler_product_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Product name'),
    '#maxlength' => 256,
    '#default_value' => variable_get('commerce_tradedoubler_product_name', '')
  );

  $form['mapping_product']['commerce_tradedoubler_main_category_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Category name (product)'),
    '#maxlength' => 256,
    '#default_value' => variable_get('commerce_tradedoubler_main_category_name', '')
  );

  $form['mapping_product']['commerce_tradedoubler_brand'] = array(
    '#type' => 'textfield',
    '#title' => t('Brand'),
    '#maxlength' => 256,
    '#default_value' => variable_get('commerce_tradedoubler_brand', '')
  );

  $form['mapping_product']['commerce_tradedoubler_product_description'] = array(
    '#type' => 'textfield',
    '#title' => t('Product description'),
    '#maxlength' => 256,
    '#default_value' => variable_get('commerce_tradedoubler_product_description', '')
  );

  $form['mapping_product']['commerce_tradedoubler_product_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Product URL'),
    '#maxlength' => 256,
    '#default_value' => variable_get('commerce_tradedoubler_product_url', '')
  );

  $form['mapping_product']['commerce_tradedoubler_product_image_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Product image URL'),
    '#maxlength' => 256,
    '#default_value' => variable_get('commerce_tradedoubler_product_image_url', '')
  );

  // Available tokens.
  $token_types = array(
    'node' => 'node'
  );
  // Set up the tokenization.
  if (module_exists('token')) {
    $form['mapping_product']['token_tree'] = array(
      '#theme' => 'token_tree',
      '#token_types' => $token_types,
    );
  }

  $form['mapping_line_item'] = array(
    '#type' => 'fieldset',
    '#title' => t('Order/line item fields mapping'),
    '#group' => 'tradedoubler'
  );

  $form['mapping_line_item']['mapping_description'] = array(
    '#markup' => '<p>' . t('Use the field tokens below to map with TradeDoubler properties.') . '</p>',
  );

  $form['mapping_line_item']['commerce_tradedoubler_li_product_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Product ID'),
    '#default_value' => variable_get('commerce_tradedoubler_li_product_id', '')
  );

  $form['mapping_line_item']['commerce_tradedoubler_li_price'] = array(
    '#type' => 'textfield',
    '#title' => t('Price'),
    '#default_value' => variable_get('commerce_tradedoubler_li_price', '')
  );

  $form['mapping_line_item']['commerce_tradedoubler_li_currency'] = array(
    '#type' => 'textfield',
    '#title' => t('Currency'),
    '#default_value' => variable_get('commerce_tradedoubler_li_currency', '')
  );

  $form['mapping_line_item']['commerce_tradedoubler_li_product_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Product name'),
    '#default_value' => variable_get('commerce_tradedoubler_li_product_name', '')
  );

  $form['mapping_line_item']['commerce_tradedoubler_li_quantity'] = array(
    '#type' => 'textfield',
    '#title' => t('Quantity'),
    '#default_value' => variable_get('commerce_tradedoubler_li_quantity', '')
  );

  // Available tokens.
  $token_types = array(
    'commerce_line_item' => 'commerce-line-item'
  );
  // Set up the tokenization.
  if (module_exists('token')) {
    $form['mapping_line_item']['token_tree'] = array(
      '#theme' => 'token_tree',
      '#token_types' => $token_types,
    );
  }

  return system_settings_form($form);
}

function commerce_tradedoubler_get_container_tags_settings() {
  $settings = array(
    'frontpage' => array(
      'title' => t('Frontpage')
    ),
    'product_listing' => array(
      'title' => t('Product listing'),
    ),
    'product' => array(
      'title' => t('Product')
    ),
    'basket' => array(
      'title' => t('Basket/Cart')
    ),
    'registration' => array(
      'title' => t('Order registration')
    ),
    'post_purchase' => array(
      'title' => t('Post purchase / "Thank you" page')
    ),
  );

  return $settings;
}

// function commerce_tradedoubler_get_settings() {
//   $settings = &drupal_static(__FUNCTION__);

//   if (!isset($settings)) {

//   }
// }
