<?php

/**
 * @file commerce_tradedoubler.scripts.inc
 * Handles commerce_tradedoubler scripts.
 */

/**
 * Gets one or all Tradedoubler scripts.
 */
function commerce_tradedoubler_get_scripts($type = NULL, $config = NULL) {
  $scripts = array();
  $config_json = json_encode($config);

  $scripts['frontpage'] = <<<EOF
    <script type="text/javascript">

    var TDConf = TDConf || {};
    TDConf.Config = {
      protocol : document.location.protocol,
      containerTagId : "{$config->containerTagId}"
    };

    if(typeof (TDConf) != "undefined"){
      TDConf.sudomain = ("https:" == document.location.protocol) ? "swrap" : "wrap";
      TDConf.host = ".tradedoubler.com/wrap";
      TDConf.containerTagURL = (("https:" == document.location.protocol) ? 
        "https://" : "http://")  + TDConf.sudomain + TDConf.host;

      if (typeof (TDConf.Config) != "undefined") {
            document.write(unescape("%3Cscript src='" + TDConf.containerTagURL  + "?id="+ TDConf.Config.containerTagId +" ' type='text/javascript'%3E%3C/script%3E"));
      }
    }
    </script>
EOF;

  $scripts['product_listing'] = <<<EOF
    <script type="text/javascript">

    var TDConf = TDConf || {};
    TDConf.Config = {$config_json};

    if(typeof (TDConf) != "undefined"){
      TDConf.sudomain = ("https:" == document.location.protocol) ? "swrap" : "wrap";
      TDConf.host = ".tradedoubler.com/wrap";
      TDConf.containerTagURL = (("https:" == document.location.protocol) ? "https://" : "http://")  + TDConf.sudomain + TDConf.host;

      if (typeof (TDConf.Config) != "undefined") {
        document.write(unescape("%3Cscript src='" + TDConf.containerTagURL  + "?id="+ TDConf.Config.containerTagId +" ' type='text/javascript'%3E%3C/script%3E"));
      }
    }
    </script>
EOF;

  $scripts['product'] = <<<EOF
    <script type="text/javascript">

    var TDConf = TDConf || {};
    TDConf.Config = {$config_json};

    if(typeof (TDConf) != "undefined"){
      TDConf.sudomain = ("https:" == document.location.protocol) ? "swrap" : "wrap";
      TDConf.host = ".tradedoubler.com/wrap";
      TDConf.containerTagURL = (("https:" == document.location.protocol) ? "https://" : "http://")  + TDConf.sudomain + TDConf.host;

      if (typeof (TDConf.Config) != "undefined") {
        document.write(unescape("%3Cscript src='" + TDConf.containerTagURL  + "?id="+ TDConf.Config.containerTagId +" ' type='text/javascript'%3E%3C/script%3E"));
      }
    }
    </script>
EOF;

  $scripts['basket'] = <<<EOF
    <script type="text/javascript">

    var TDConf = TDConf || {};
    TDConf.Config = {$config_json};

    if(typeof (TDConf) != "undefined"){
      TDConf.sudomain = ("https:" == document.location.protocol) ? "swrap" : "wrap";
      TDConf.host = ".tradedoubler.com/wrap";
      TDConf.containerTagURL = (("https:" == document.location.protocol) ? "https://" : "http://")  + TDConf.sudomain + TDConf.host;

      if (typeof (TDConf.Config) != "undefined") {
        document.write(unescape("%3Cscript src='" + TDConf.containerTagURL  + "?id="+ TDConf.Config.containerTagId +" ' type='text/javascript'%3E%3C/script%3E"));
      }
    }
    </script>
EOF;

  $scripts['registration'] = <<<EOF
    <script type="text/javascript">

    var TDConf = TDConf || {};
    TDConf.Config = {
      protocol : document.location.protocol,
      containerTagId : "{$config->containerTagId}"
    };

    if(typeof (TDConf) != "undefined"){
      TDConf.sudomain = ("https:" == document.location.protocol) ? "swrap" : "wrap";
      TDConf.host = ".tradedoubler.com/wrap";
      TDConf.containerTagURL = (("https:" == document.location.protocol) ? 
        "https://" : "http://")  + TDConf.sudomain + TDConf.host;

      if (typeof (TDConf.Config) != "undefined") {
            document.write(unescape("%3Cscript src='" + TDConf.containerTagURL  + "?id="+ TDConf.Config.containerTagId +" ' type='text/javascript'%3E%3C/script%3E"));
      }
    }
    </script>
EOF;

  $scripts['post_purchase'] = <<<EOF
    <script type="text/javascript">

    var TDConf = TDConf || {};
    TDConf.Config = {$config_json};

    if(typeof (TDConf) != "undefined"){
      TDConf.sudomain = ("https:" == document.location.protocol) ? "swrap" : "wrap";
      TDConf.host = ".tradedoubler.com/wrap";
      TDConf.containerTagURL = (("https:" == document.location.protocol) ? "https://" : "http://")  + TDConf.sudomain + TDConf.host;

      if (typeof (TDConf.Config) != "undefined") {
        document.write(unescape("%3Cscript src='" + TDConf.containerTagURL  + "?id="+ TDConf.Config.containerTagId +" ' type='text/javascript'%3E%3C/script%3E"));
      }
    }
    </script>
EOF;

  if (isset($type)) {
    return $scripts[$type];
  }

  return $scripts;
}