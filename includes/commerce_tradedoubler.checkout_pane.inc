<?php

/**
 * Implements base_settings_form()
 */
function commerce_tradedoubler_tp_settings_form($checkout_pane) {
  $form = array();
  
  $form['commerce_tradedoubler_px_event'] = array(
    '#type' => 'textfield',
    '#title' => 'event',
    '#description' => t('Provided by TradeDoubler, this parameter is linked to our organization.'),
    '#default_value' => variable_get('commerce_tradedoubler_px_event')
  );

  $form['commerce_tradedoubler_px_currency'] = array(
    '#type' => 'textfield',
    '#title' => 'currency',
    '#description' => t('A three letter currency code for the currency in which the sale was made.'),
    '#default_value' => variable_get('commerce_tradedoubler_px_currency')
  );
  $form['commerce_tradedoubler_px_organization'] = array(
    '#type' => 'textfield',
    '#title' => 'organization',
    '#description' => t('Your TradeDoubler organization ID, provided by TradeDoubler.'),
    '#default_value' => variable_get('commerce_tradedoubler_px_organization')
  );

  $form['commerce_tradedoubler_px_orderNumber'] = array(
    '#type' => 'textfield',
    '#title' => 'orderNumber',
    '#description' => t('Unique for each order.'),
    '#default_value' => variable_get('commerce_tradedoubler_px_orderNumber')
  );
  $form['commerce_tradedoubler_px_orderValue'] = array(
    '#type' => 'textfield',
    '#title' => 'orderValue',
    '#description' => t('The total value of an order.'),
    '#default_value' => variable_get('commerce_tradedoubler_px_orderValue')
  );
  $form['commerce_tradedoubler_secretcode'] = array(
    '#type' => 'textfield',
    '#title' => 'Secret code',
    '#description' => t('TradeDoubler provides the secret code to calculate the checksum using md5 hash.'),
    '#default_value' => variable_get('commerce_tradedoubler_secretcode')
  );
  
  $token_types = array(
    'commerce-order'
  );
  if (module_exists('token')) {
    $form['token_tree'] = array(
      '#theme' => 'token_tree',
      '#token_types' => $token_types,
    );
  }
  return $form;
}

/**
 * Implements base_checkout_form()
 */
function commerce_tradedoubler_tp_checkout_form($form, $form_state, $checkout_pane, $order) {
  if ($tduid = $_COOKIE['TRADEDOUBLER']) {
    $items = array('event', 'currency', 'organization', 'orderNumber', 'orderValue', 'tduid', 'checksum');
    $params = array();

    // Tokens
    $token_data = array(
      'commerce_order' => $order
    );

    // Build array of values
    // @todo Why does not #tree work?
    foreach ($items AS $item) {
      if ($setting = variable_get('commerce_tradedoubler_px_' . $item, FALSE)) {
        $params[$item] = token_replace($setting, $token_data);
      }
    }
    // Set TD uid
    $params['tduid'] = $_COOKIE['TRADEDOUBLER'];

    // Let's get ourselves a checksum
    $secretcode = variable_get('commerce_tradedoubler_secretcode', '');
    $checksum = "v04" . md5($secretcode . $params['orderNumber'] . $params['orderValue']);
    $params['checksum'] = $checksum;

    // Remove tradedoubler cookie
    // @TODO Should we really do this?
    setcookie('TRADEDOUBLER', '', time() - 56577677200,'/');
    
    // Build URL
    $url = url('https://tbs.tradedoubler.com/report', array('query' => $params, 'external' => TRUE));
    $markup = '<img src="' . $url . '">';

    // Return simple markup with image.
    $form = array();
    $form['commerce_tradedoubler_track'] = array(
      '#markup' => $markup,
    );
    return $form;
  }
}