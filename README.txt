Commerce TradeDoubler Advertisers Integration
=============================================

Introduction
------------

This module was designed to provide integration with TradeDoubler advertisers program.
It automatically inserts all the necessary TD track scripts/tags and the callback page. It is fully customizable.

Usage
-----

The default settings are suited to a default Drupal Commerce install but if needed, all settings can be changed at [site]/admin/commerce/config/tradedoubler.
There's also an hook to be possible to alter container tags config object used on the generated script tags.

Dependencies
------------

- Drupal Commerce
  - Commerce Cart
  - Commerce Checkout
  - Commerce Product Reference
- Token

Developed by Marco Fernandes (https://drupal.org/user/2127558)